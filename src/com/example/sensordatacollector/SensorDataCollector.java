package com.example.sensordatacollector;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.UUID;
import java.util.Vector;
import java.util.regex.Pattern;


public class SensorDataCollector extends Activity {
	  class MutableInt {
		  int value = 0; 
		  public void increment () { ++value;      }
		  public int  get ()       { return value; }
	  }
	  private SensorManager mSensorManager = null;
	  public NetworkInfo mWifi;
	  //data structures for holding sensor data for each sensor listeners
	  public Vector<SensorEventListener> listeners = new Vector<SensorEventListener>();
	  public SparseArray<BufferedWriter> filepointers = new SparseArray<BufferedWriter>();
	  public SparseArray<MutableInt> filesize = new SparseArray<MutableInt>();
	  public List<Sensor> deviceSensors;
	  
	  //global state variables
	  public int LOG_SIZE = 25000 ;
	  public boolean deviceinfosent=false;
	  public String sensorinfo="";
	  public String deviceId="";
	  public boolean started=false;
	
	  
	  
	  @Override
	  //What action to take on creating the app
	  public void onCreate(Bundle savedInstanceState) {
		  super.onCreate(savedInstanceState);
		  setContentView(R.layout.activity_main);
	   
		  Button start = (Button)findViewById(R.id.startButton);
		  Button stop = (Button)findViewById(R.id.stopButton);
		  Button exit = (Button)findViewById(R.id.exitButton);
	    
		  start.setOnClickListener(new OnClickListener() {
			public void onClick(View v){
				if(!started)startService();
				else updateServiceStatus(2);
			}
		  });  
		
		  stop.setOnClickListener(new OnClickListener() {
			public void onClick(View v){
				if(started)stopService();
				else updateServiceStatus(4);
			}
		  });          
		
		  exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v){
				stopService();
				filepointers=null;
				filesize=null;
				deviceSensors=null;
				listeners=null;
				mWifi=null;
				mSensorManager=null;
				System.exit(0);
			}
		  });  
		  
		  try{
			  File path = Environment.getExternalStorageDirectory();
			  File fpt = new File(path,"state.txt");
			  if (fpt.exists()){
				  fpt.delete(); 
			  }
			  savestate();
			  
			  final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
			  final String tmDevice, tmSerial, androidId;
			  tmDevice = "" + tm.getDeviceId();
			  tmSerial = "" + tm.getSimSerialNumber();
			  androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

			  UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
			  deviceId = deviceUuid.toString();
			     
		  }catch (Exception e) {
			  e.printStackTrace();
		  }  
	  }
	  
	  @Override
	  //What action to take on resuming the app
	  protected void onResume() {
	    super.onResume();
	    loadstate();
	    ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
	    mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	    
	    if(started)
	    	updateServiceStatus(2);
	    else
	    	updateServiceStatus(4);
	  }

	  @Override
	  //What action to take on pausing the app
	  protected void onPause() {
	    super.onPause();
	    savestate();
	  }
	  
	  @Override
	  //what action to take on stopping the app
	  protected void onStop() {
	    super.onStop();
	  }

	  @Override
	  protected void onDestroy(){
	      super.onDestroy();
	      stopService();
	  }
	  
	  
	  //Start Service
	  private void startService(){
		// get SensorManager instance.
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
		//list the sensors in the device
		if(!deviceinfosent){
			sensorinfo="";
			for (int i=0; i<deviceSensors.size()-1; i++){
				Sensor temp=deviceSensors.get(i);
				String info= "Name="+temp.getName()+"\nType="+temp.getType()+"\nVersion="+temp.getVersion()+
						"\nResolution="+temp.getResolution()+"\nVendor="+temp.getVendor()+"\nPower="+temp.getPower()+
						"\nMinDelay="+temp.getMinDelay()+"\nSensor Range="+temp.getMaximumRange();
				sensorinfo+=info+"\n\n";
			}
		}
		// initialize sensor listeners
		initFiles();
		initListeners();
		started=true;
		updateServiceStatus(1);
	  }
	  
	  //Stop Service
	  private void stopService() {
		  try{
			  unregisterListeners();
			  closeFiles();
			  for (int i=0; i<deviceSensors.size()-1; i++){
				  sendSensorData(deviceSensors.get(i).getType());
			  }
			  filepointers.clear();
			  filesize.clear();
			  started=false;
			  File path = Environment.getExternalStorageDirectory();
			  File fpt = new File(path,"state.txt");
			  if (fpt.exists()){
				  fpt.delete(); 
			  }
			  updateServiceStatus(3);
		  }catch (Exception e) {
			  e.printStackTrace();
		  }
	  }
	  
	  //unregister Listeners
	  private void unregisterListeners() {
		  for (SensorEventListener item : listeners) {
			  if(mSensorManager!=null)mSensorManager.unregisterListener(item);
		  }
		  listeners.clear();
	   }
	  
	 //Display current status
	  private void updateServiceStatus(int val) {
		  TextView status = (TextView)findViewById(R.id.statustext);
    	  if(val==1) status.setText("Service Running.");
    	  else if (val==2) status.setText("Service Already Running!");
    	  else if (val==3) status.setText("Service Stopped.");
    	  else if (val==4) status.setText("Service Not Running!");
      }
	  
	  //save states into file
	  public void savestate(){
		 File path = Environment.getExternalStorageDirectory();
		 File fpt = new File(path,"state.txt");
		 try {
			BufferedWriter out = new BufferedWriter(new FileWriter(fpt));
			if(started)out.write("1\n");
			else out.write("0\n");
			if(deviceinfosent)out.write("1\n");
			else out.write("0\n");
			out.close();	
		  } 
		  catch (IOException e) {
				e.printStackTrace();
		  }
	  }
	 
	  //load states from file
	  public void loadstate(){
		 try{
			  File path = Environment.getExternalStorageDirectory();
			  File fpt = new File(path,"state.txt");
			  FileInputStream fstream = new FileInputStream(new File(fpt.getAbsolutePath()) );
			  // Get the object of DataInputStream
			  DataInputStream in = new DataInputStream(fstream);
			  BufferedReader br = new BufferedReader(new InputStreamReader(in));
			  String strLine;
			  int linecnt=0;
			  while ((strLine = br.readLine()) != null)   {
				  linecnt++;
				  if (linecnt==1){
					  if(Integer.parseInt(strLine)==1)  started=true;
					  else  started=false;
				  }
				  if(linecnt==2){
					  if(Integer.parseInt(strLine)==1)  deviceinfosent=true;
					  else  deviceinfosent=false;
				  }
			  }
			  in.close();
			}catch (IOException e){
			    	e.printStackTrace();
			}
	  }
	 
	  // This function registers sensor listeners for the different sensors
	  public void initListeners(){
		  if(mSensorManager!=null){
		    listeners.clear();
		    int size = deviceSensors.size();
		    for (int i=0; i<size-1; i++)
		    {
		    	mSensorManager.registerListener(mSensorListener,
			            mSensorManager.getDefaultSensor(deviceSensors.get(i).getType()),
			            SensorManager.SENSOR_DELAY_FASTEST);  //may want to change delay parameter to SENSOR_DELAY_FASTEST if needed
		    	listeners.add(mSensorListener);
		    }   
		  }
	  }
	 
	  
	  public void initFiles(){
		  if(mSensorManager!=null){
		    filepointers.clear();
		    filesize.clear();
		    for (int i=0; i<deviceSensors.size()-1; i++)
		    {
		    	String logFileName = String.format("LOG%d"+"_"+deviceId+"_"+Build.PRODUCT+"_"+Build.MODEL+"_@%d.csv",System.currentTimeMillis(),deviceSensors.get(i).getType());
				File path = Environment.getExternalStorageDirectory();
				try {
					BufferedWriter out = new BufferedWriter(new FileWriter(new File(path, logFileName)));
					filepointers.put(deviceSensors.get(i).getType(),out);
					filesize.put(deviceSensors.get(i).getType(),new MutableInt());
				}catch (Exception e) {
					e.printStackTrace();
				}	
		    }   
		  }
	  }
	  
	  public void closeFiles(){
		  if(mSensorManager!=null) {
		    for (int i=0; i<deviceSensors.size()-1; i++)
		    {
		    	try {
		    		BufferedWriter out=filepointers.get(deviceSensors.get(i).getType());
		    		if(out!=null)out.close();
				}catch (Exception e) {
					e.printStackTrace();
				}	
		    }   
		  }
	  }
	  
	  public void resetFile(int type){

		    String logFileName = String.format("LOG%d"+"_"+deviceId+"_"+Build.PRODUCT+"_"+Build.MODEL+"_@%d.csv",System.currentTimeMillis(),type);
			File path = Environment.getExternalStorageDirectory();
			try {
				BufferedWriter out = new BufferedWriter(new FileWriter(new File(path, logFileName)));
				filepointers.put(type,out);
				filesize.put(type,new MutableInt());
			}catch (Exception e) {
				e.printStackTrace();
			}	
		  
	  }
	  //define a sensor event handler will be called when some sensor events are tiggered
	  private SensorEventListener mSensorListener = new SensorEventListener() {
	    @Override
	    public void onAccuracyChanged(Sensor sensor, int accuracy) {
	      // when accuracy changed, this method will be called.
	    }
	    @Override
	    // when sensor value is changed, this method will be called.
	    public void onSensorChanged(SensorEvent event) {
	        // details in http://developer.android.com/reference/android/hardware/SensorEvent.html
	    	try {
	    		if(filepointers!=null)
	    		{
	    			BufferedWriter out=filepointers.get(event.sensor.getType());
					if(!deviceinfosent){
						if(out!=null)out.write(sensorinfo);
						sensorinfo="";
						deviceinfosent=true;
					}
					if(out!=null){
						if(event.values.length==3)
							out.write(String.format("%d,%d,%f,%f,%f\n",System.currentTimeMillis(),event.sensor.getType(),event.values[0],event.values[1],event.values[2]));
						else
							out.write(String.format("%d,%d,%f\n",System.currentTimeMillis(),event.sensor.getType(),event.values[0]));
						out.flush();
					}
					MutableInt count = filesize.get(event.sensor.getType());
					if (count != null) {
						count.increment();
						if (count.get()==LOG_SIZE) {
							out.close();
			          		sendSensorData(event.sensor.getType());
			          		resetFile(event.sensor.getType());
			          		//System.out.println("Records="+count.get());
			          	}
					}
					//else 
						//System.out.println("Not initialized");
	    		}
			}catch (IOException e) {
				e.printStackTrace();
			}	   
	    }
	  };
	  
	  
	  public void sendSensorData(int type){
			try 
			{	
				if (mWifi.isConnected()) {
					String myDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
					File f = new File(myDirectory);
					File[] flists=null;
					if(f.exists() && f.isDirectory()){
						final Pattern p = Pattern.compile("LOG.*@"+type+"\\.csv"); 
						flists = f.listFiles(new FileFilter(){
							@Override
							public boolean accept(File file) {
								return p.matcher(file.getName()).matches();
							}
						});
					}
					//System.out.println("found "+flists.length+" files");
					for(int i=0;i<flists.length;i++){
						new UpladFilesTask().execute(flists[i]);	//start asynchronous upload	 // Do whatever
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}	    
	  }
	  
	  
	  
	  ////////////////////////////////////////////////Asynchronous Upload task///////////////////////////////////////////////
	  private class UpladFilesTask extends AsyncTask<File, Integer, String> {

			private String sendData(File file)
			{			
				HttpURLConnection connection = null;
				DataOutputStream outputStream = null;
				
				String pathToOurFile = file.getAbsolutePath();
				String fileName = file.getName();

				String urlServer = "http://128.174.241.211:8080/data";
				String lineEnd = "\r\n";
				String twoHyphens = "--";
				String boundary =  "*****";

				int bytesRead, bytesAvailable, bufferSize;
				byte[] buffer;
				int maxBufferSize = 1*1024*1024;
				
				try
				{
					FileInputStream fileInputStream = new FileInputStream(new File(pathToOurFile) );
		
					URL url = new URL(urlServer);
					connection = (HttpURLConnection) url.openConnection();
		
					// Allow Inputs & Outputs
					connection.setDoInput(true);
					connection.setDoOutput(true);
					connection.setUseCaches(false);
		
					// Enable POST method
					connection.setRequestMethod("POST");
		
					connection.setRequestProperty("Connection", "Keep-Alive");
					connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
		
					outputStream = new DataOutputStream( connection.getOutputStream() );
					outputStream.writeBytes(twoHyphens + boundary + lineEnd);
					outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + fileName +"\"" + lineEnd);
					outputStream.writeBytes(lineEnd);
		
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					buffer = new byte[bufferSize];
		
					// Read file
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
		
					while (bytesRead > 0)
					{
						outputStream.write(buffer, 0, bufferSize);
						bytesAvailable = fileInputStream.available();
						bufferSize = Math.min(bytesAvailable, maxBufferSize);
						bytesRead = fileInputStream.read(buffer, 0, bufferSize);
					}
		
					outputStream.writeBytes(lineEnd);
					outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
		
					// Responses from the server (code and message)
					int serverResponseCode = connection.getResponseCode();
					String serverResponseMessage = connection.getResponseMessage();
		
					fileInputStream.close();
					outputStream.flush();
					outputStream.close();
					
					file.delete(); //delete log file
					//System.out.println("File uploaded and deleted "+deleted);
					return "Success";
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
				return "Error";
			} 
			@Override
			protected String doInBackground(File... file) {
				if (file[0].exists())
					return sendData(file[0]);	
				else 
					return "No file";
			}
		    
			// This is called when doInBackground() is finished
		    protected void onPostExecute(String result) {
		    	//System.out.print("\n<<---------Upload status +"+result+"\n");
		    }
		};
	  
	}